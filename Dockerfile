FROM eclipse-temurin:17-jre-alpine

HEALTHCHECK --interval=30s --retries=3 --timeout=10s --start-period=40s CMD curl -f http://localhost:8081/order-management/v1/actuator/health || exit 1;

WORKDIR /app

COPY ./build/libs/application.jar /app/application.jar

# Expose the port that the Spring Boot application will run on
EXPOSE 8081

# Specify the command to run the Spring Boot application when the container starts
CMD ["java", "-jar", "application.jar"]