package com.deepmetis.application.controller;

import static org.springframework.http.HttpStatus.CREATED;

import com.deepmetis.application.api.OrdersApi;
import com.deepmetis.application.model.Order;
import com.deepmetis.application.model.ProcessedOrder;
import com.deepmetis.application.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class OrderController implements OrdersApi {

  private final OrderService orderService;

  @Autowired
  public OrderController(OrderService orderService) {
    this.orderService = orderService;
  }

  @Override
  public ResponseEntity<ProcessedOrder> createOrder(Order order) {
    return new ResponseEntity<>(orderService.createOrder(order), CREATED);
  }
}
