package com.deepmetis.application.controller;

import com.deepmetis.application.api.IngredientsApi;
import com.deepmetis.application.model.Ingredients;
import com.deepmetis.application.model.ListIngredientsInputParameter;
import com.deepmetis.application.service.IngredientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class IngredientController implements IngredientsApi {

  private final IngredientService ingredientService;

  @Autowired
  public IngredientController(IngredientService ingredientService) {
    this.ingredientService = ingredientService;
  }

  @Override
  public ResponseEntity<Ingredients> listIngredients(ListIngredientsInputParameter input) {
    return ResponseEntity.ok(ingredientService.listIngredients());
  }
}
