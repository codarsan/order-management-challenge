package com.deepmetis.application;

import com.deepmetis.application.model.IngredientEntity;
import com.deepmetis.application.repository.IngredientRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class DataInitializer implements CommandLineRunner {

  private final IngredientRepository ingredientRepository;

  @Autowired
  public DataInitializer(IngredientRepository ingredientRepository) {
    this.ingredientRepository = ingredientRepository;
  }

  @Override
  public void run(String... args) {
    var meat = new IngredientEntity();
    meat.setName("Meat");
    meat.setQuantity(9);
    meat.setPrice(12000);
    meat.setType("Cooked");

    var lettuce = new IngredientEntity();
    lettuce.setName("Lettuce");
    lettuce.setQuantity(20);
    lettuce.setPrice(1000);
    lettuce.setType("Raw vegetable");

    ingredientRepository.saveAll(List.of(meat, lettuce));
  }
}