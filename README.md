# Subway Sandwich Order Management challenge

My solution is an attempt to a simple and clean architecture.
The following was implemented:
- an API first approach was used
- Rest controllers (along with tests) for the Ingredients and Orders API
- a gradle Module per domain
- linting was configured
- a gitlab-ci pipeline that builds, tests and then deploys an image to Gitlab Container Registry
- an in-memory database is used for development purpose


## Getting Started
1- Import in Intellij

2- Run build and tests:  
```gradle build```

3- Run the application  
```gradle bootRun```