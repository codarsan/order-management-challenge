package com.deepmetis.application.service;

import com.deepmetis.application.model.Ingredients;

public interface IngredientService {

  Ingredients listIngredients();

}
