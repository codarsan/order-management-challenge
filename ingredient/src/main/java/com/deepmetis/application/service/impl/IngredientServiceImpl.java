package com.deepmetis.application.service.impl;

import com.deepmetis.application.mapper.IngredientMapper;
import com.deepmetis.application.model.Ingredient;
import com.deepmetis.application.model.Ingredients;
import com.deepmetis.application.repository.IngredientRepository;
import com.deepmetis.application.service.IngredientService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class IngredientServiceImpl implements IngredientService {

  private final IngredientRepository repository;

  private final IngredientMapper mapper;

  @Autowired
  public IngredientServiceImpl(IngredientRepository repository, IngredientMapper mapper) {
    this.repository = repository;
    this.mapper = mapper;
  }

  @Override
  public Ingredients listIngredients() {
    var entities = repository.findAll();
    List<Ingredient> ingredients = mapper.toModel(entities);
    return new Ingredients().count(ingredients.size()).data(ingredients);
  }

}
