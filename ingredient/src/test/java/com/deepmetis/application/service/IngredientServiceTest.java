package com.deepmetis.application.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doReturn;

import com.deepmetis.application.mapper.IngredientMapper;
import com.deepmetis.application.mapper.IngredientMapperImpl;
import com.deepmetis.application.model.IngredientEntity;
import com.deepmetis.application.model.Ingredients;
import com.deepmetis.application.repository.IngredientRepository;
import com.deepmetis.application.service.impl.IngredientServiceImpl;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class IngredientServiceTest {

  private static final String LETTUCE = "Lettuce";
  private static final String MEAT = "Meat";

  @Mock
  private IngredientRepository repository;

  private IngredientService ingredientService;

  @BeforeEach
  void setUp() {
    IngredientMapper mapper = new IngredientMapperImpl();
    ingredientService = new IngredientServiceImpl(repository, mapper);
  }

  @Test
  void listIngredients_should_return_an_empty_list() {
    noIngredients();

    Ingredients result = ingredientService.listIngredients();

    assertEquals(0, result.getCount());
    assertEquals(0, result.getData().size());
  }

  private void noIngredients() {
    doReturn(List.of()).when(repository).findAll();
  }

  @Test
  void listIngredients_should_map_one_element_correctly() {
    oneIngredient();

    Ingredients result = ingredientService.listIngredients();

    assertEquals(1, result.getCount());
    assertEquals(1, result.getData().size());
    var foundIngredient = result.getData().get(0);
    assertEquals(100, foundIngredient.getId());
    assertEquals("Meat", foundIngredient.getName());
    assertEquals(500, foundIngredient.getPrice());
    assertEquals(2, foundIngredient.getQuantity());
  }

  private void oneIngredient() {
    IngredientEntity meat = new IngredientEntity();
    meat.setId(100);
    meat.setName("Meat");
    meat.setPrice(500);
    meat.setQuantity(2);
    doReturn(List.of(meat)).when(repository).findAll();
  }

  @Test
  void listIngredients_should_map_multiple_elements_correctly() {
    multipleIngredients();

    Ingredients result = ingredientService.listIngredients();

    assertEquals(2, result.getCount());
    assertEquals(2, result.getData().size());

    var meat = result.getData().get(0);
    assertEquals(MEAT, meat.getName());
    assertEquals(100, meat.getId());
    assertEquals(500, meat.getPrice());
    assertEquals(2, meat.getQuantity());

    var lettuce = result.getData().get(1);
    assertEquals(LETTUCE, lettuce.getName());
    assertEquals(200, lettuce.getId());
    assertEquals(70, lettuce.getPrice());
    assertEquals(10, lettuce.getQuantity());
  }

  private void multipleIngredients() {
    IngredientEntity meat = new IngredientEntity();
    meat.setId(100);
    meat.setName(MEAT);
    meat.setPrice(500);
    meat.setQuantity(2);

    IngredientEntity lettuce = new IngredientEntity();
    lettuce.setId(200);
    lettuce.setName(LETTUCE);
    lettuce.setPrice(70);
    lettuce.setQuantity(10);

    doReturn(List.of(meat, lettuce)).when(repository).findAll();
  }
}