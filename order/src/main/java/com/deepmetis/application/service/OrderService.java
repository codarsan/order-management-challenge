package com.deepmetis.application.service;

import com.deepmetis.application.model.Order;
import com.deepmetis.application.model.ProcessedOrder;

public interface OrderService {

  ProcessedOrder createOrder(Order order);
}
