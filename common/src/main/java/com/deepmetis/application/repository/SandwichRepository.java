package com.deepmetis.application.repository;

import com.deepmetis.application.model.SandwichEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SandwichRepository extends JpaRepository<SandwichEntity, Integer> {
}
