package com.deepmetis.application.model;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Embedded;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import java.util.HashSet;
import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EntityListeners(AuditingEntityListener.class)
public class SandwichEntity {

  @Id
  @GeneratedValue
  private Integer id;

  @Embedded
  private final AuditMetadata auditMetadata = new AuditMetadata();

  @Column
  private String label;

  @ManyToMany(cascade = {
      CascadeType.PERSIST,
      CascadeType.MERGE
  })
  @JoinTable(name = "sandwich_ingredient",
      joinColumns = @JoinColumn(name = "sandwich_id"),
      inverseJoinColumns = @JoinColumn(name = "ingredient_id")
  )
  private Set<IngredientEntity> ingredients = new HashSet<>();

  @Column
  private Integer cost;

}
