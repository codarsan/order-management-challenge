package com.deepmetis.application.model;

import jakarta.persistence.Column;
import jakarta.persistence.Embedded;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToMany;
import java.util.HashSet;
import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EntityListeners(AuditingEntityListener.class)
public class IngredientEntity {

  @Id
  @GeneratedValue
  private Integer id;

  @Embedded
  private final AuditMetadata auditMetadata = new AuditMetadata();

  @Column
  private String name;

  @Column
  private String type;

  @Column
  private Integer price;

  @Column
  private Integer quantity;

  @ManyToMany(mappedBy = "ingredients")
  private Set<SandwichEntity> sandwiches = new HashSet<>();
}
