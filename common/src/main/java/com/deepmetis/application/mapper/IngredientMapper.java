package com.deepmetis.application.mapper;

import static org.mapstruct.ReportingPolicy.IGNORE;

import com.deepmetis.application.model.Ingredient;
import com.deepmetis.application.model.IngredientEntity;
import java.util.List;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", unmappedTargetPolicy = IGNORE,
    suppressTimestampInGenerated = true)
public interface IngredientMapper {

  Ingredient toModel(IngredientEntity entity);

  List<Ingredient> toModel(List<IngredientEntity> entities);
}
